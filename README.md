# Safe States Alliance
## WordPress Starter Theme
### Intro
This repository is intended to give web development teams working for the Safe States Alliance a head-start on creating WordPress websites by providing a bare-bones starter theme which incorporates Safe States' branding.
The theme itself is a child theme of [Genesis Framework](https://my.studiopress.com/themes/genesis/)

The project consists of the Safe States theme and two additional, required assets:
- [The Genesis Framework Theme](https://my.studiopress.com/themes/genesis/)
- [One Click Demo Import Plugin](https://wordpress.org/plugins/one-click-demo-import/)

### Installation
1. Create a new WordPress installation if you do not have one already 
2. Download or clone this repository.
3. Move the repository's ```themes``` and ```plugins``` directories to your WordPress installation's ```wp-content``` directory.
4. In your WordPress dashboard, activate the 'Safe States' theme and the 'One Click Demo Import' plugin
5. While still in your dashboard, navigate to "Appearance > Import Demo Data" and then click "Import Demo Data"
    1. This will set-up widgets, customizer settings, navigation menus, and a sample post and page
    2. It may be necessary to re-assign navigation menus to their appropriate locations after running the import.
        - The Primary Menu location (blue) should use the "Primary Navigation Menu". This mirrors the top-level navigation found on [www.safestates.org](http://www.safestates.org)
        - The Internal Navigation location (green/teal) is intended for the internal navigation around the site you are creating
        - The "Header Menu" mirrors the header navigation on the main Safe States website
        - The "Connect" and "Quick Links" menus mirror the footer links on the main Safe States website
        
### Demo
A demo of the starter theme can be found at [https://safestates.rpsdigitallab.com/](https://safestates.rpsdigitallab.com/)

### Resources
Resources for the Genesis Framework:
- [StudioPress: Genesis Code Snippets](https://my.studiopress.com/customization/snippets/)


- [Carrie Dils: Genesis Hook Reference](https://carriedils.com/genesis-hook-reference/)

- [Genesis Visual Hook Guide](https://wordpress.org/plugins/genesis-visual-hook-guide/) (WordPress Plugin)

- [Genesis Simple Hooks](https://wordpress.org/plugins/genesis-simple-hooks/) (WordPress Plugin)

- [Genesis Visual Hook Guide - Website](https://genesistutorials.com/visual-hook-guide/)

