<?php

add_action('genesis_setup','child_theme_setup', 15);
function child_theme_setup() {

	define( 'CHILD_THEME_VERSION', filemtime( get_stylesheet_directory() . '/style.css' ) );

	// ** Backend **

	// Image Sizes
	// add_image_size( 'be_featured', 400, 100, true );

	// Structural Wraps
	add_theme_support( 'genesis-structural-wraps', array( 'header', 'nav', 'subnav', 'inner', 'footer-widgets', 'footer' ) );

	// Menus
	add_theme_support( 'genesis-menus', array( 'primary' => 'Primary Navigation Menu' ) );

	// Sidebars
	unregister_sidebar( 'sidebar-alt' );
	//genesis_register_sidebar( array( 'name' => 'Blog Sidebar', 'id' => 'blog-sidebar' ) );
	//add_theme_support( 'genesis-footer-widgets', 4 );

	// Remove Unused Page Layouts
	genesis_unregister_layout( 'content-sidebar-sidebar' );
	genesis_unregister_layout( 'sidebar-sidebar-content' );
	genesis_unregister_layout( 'sidebar-content-sidebar' );

	// Remove Unused User Settings
	add_filter( 'user_contactmethods', 'be_contactmethods' );
	add_action( 'admin_init', 'be_remove_user_settings' );

	// Editor Styles
	add_editor_style( 'editor-style.css' );

	// Setup Theme Settings
	//include_once( CHILD_DIR . '/lib/functions/child-theme-settings.php' );

	// Reposition Genesis Metaboxes
	remove_action( 'admin_menu', 'genesis_add_inpost_seo_box' );
	add_action( 'admin_menu', 'be_add_inpost_seo_box' );
	remove_action( 'admin_menu', 'genesis_add_inpost_layout_box' );
	add_action( 'admin_menu', 'be_add_inpost_layout_box' );

	// Remove Genesis Widgets
	add_action( 'widgets_init', 'be_remove_genesis_widgets', 20 );

	// Remove Genesis Theme Settings Metaboxes
	add_action( 'genesis_theme_settings_metaboxes', 'be_remove_genesis_metaboxes' );

	// Don't update theme
	add_filter( 'http_request_args', 'be_dont_update_theme', 5, 2 );

	// ** Frontend **

	// Remove Edit link
	add_filter( 'genesis_edit_post_link', '__return_false' );

	// Responsive Meta Tag
	add_action( 'genesis_meta', 'be_viewport_meta_tag' );

	// Footer
	remove_action( 'genesis_footer', 'genesis_do_footer' );
	add_action( 'genesis_footer', 'be_footer' );

	remove_action( 'genesis_header','genesis_do_header' );
	add_action( 'genesis_header', 'themeprefix_genesis_do_header' );
}

function themeprefix_genesis_do_header() {

 global $wp_registered_sidebars;

 genesis_markup( array(
 'html5' => '<div %s>',
 'xhtml' => '<div id="title-area">',
 'context' => 'title-area',
 ) );
 do_action( 'genesis_site_title' );
 do_action( 'genesis_site_description' );
 echo '</div>';

	genesis_widget_area( 'header-middle', array(
	'before' => '<div class="header-middle widget-area">',
	'after'  => '</div>',
	) );

	if ( ( isset( $wp_registered_sidebars['header-right'] ) && is_active_sidebar( 'header-right' ) ) || has_action( 'genesis_header_right' ) ) {

 genesis_markup( array(
 'html5' => '<div %s>' . genesis_sidebar_title( 'header-right' ),
 'xhtml' => '<div class="widget-area header-widget-area">',
 'context' => 'header-widget-area',
 ) );

 do_action( 'genesis_header_right' );
 add_filter( 'wp_nav_menu_args', 'genesis_header_menu_args' );
 add_filter( 'wp_nav_menu', 'genesis_header_menu_wrap' );
 dynamic_sidebar( 'header-right' );
 remove_filter( 'wp_nav_menu_args', 'genesis_header_menu_args' );
 remove_filter( 'wp_nav_menu', 'genesis_header_menu_wrap' );

 echo '</div>';

 }

}

/*
 * Register Additional Nav Menu for Internal Navigation
 */
function be_register_additional_menu() {

	register_nav_menu( 'secondary-menu' ,__( 'Secondary Navigation Menu' ));

}
add_action( 'init', 'be_register_additional_menu' );

add_action( 'genesis_before_content_sidebar_wrap', 'add_secondary_nav_genesis' );

function add_secondary_nav_genesis() {

	wp_nav_menu( array( 'theme_location' => 'secondary-menu', 'container_class' => 'genesis-nav-menu', 'container_id' => 'secondary-nav' ) );

}


// ** Backend Functions ** //

/**
 * Customize Contact Methods
 * @since 1.0.0
 *
 * @author Bill Erickson
 * @link http://sillybean.net/2010/01/creating-a-user-directory-part-1-changing-user-contact-fields/
 *
 * @param array $contactmethods
 * @return array
 */
function be_contactmethods( $contactmethods ) {
	unset( $contactmethods['aim'] );
	unset( $contactmethods['yim'] );
	unset( $contactmethods['jabber'] );

	return $contactmethods;
}

/**
 * Remove Use Theme Settings
 *
 */
function be_remove_user_settings() {
	remove_action( 'show_user_profile', 'genesis_user_options_fields' );
	remove_action( 'edit_user_profile', 'genesis_user_options_fields' );
	remove_action( 'show_user_profile', 'genesis_user_archive_fields' );
	remove_action( 'edit_user_profile', 'genesis_user_archive_fields' );
	remove_action( 'show_user_profile', 'genesis_user_seo_fields' );
	remove_action( 'edit_user_profile', 'genesis_user_seo_fields' );
	remove_action( 'show_user_profile', 'genesis_user_layout_fields' );
	remove_action( 'edit_user_profile', 'genesis_user_layout_fields' );
}

/**
 * Register a new meta box to the post / page edit screen, so that the user can
 * set SEO options on a per-post or per-page basis.
 *
 * @category Genesis
 * @package Admin
 * @subpackage Inpost-Metaboxes
 *
 * @since 0.1.3
 *
 * @see genesis_inpost_seo_box() Generates the content in the meta box
 */
function be_add_inpost_seo_box() {

	if ( genesis_detect_seo_plugins() )
		return;

	foreach ( (array) get_post_types( array( 'public' => true ) ) as $type ) {
		if ( post_type_supports( $type, 'genesis-seo' ) )
			add_meta_box( 'genesis_inpost_seo_box', __( 'Theme SEO Settings', 'genesis' ), 'genesis_inpost_seo_box', $type, 'normal', 'default' );
	}

}

/**
 * Register a new meta box to the post / page edit screen, so that the user can
 * set layout options on a per-post or per-page basis.
 *
 * @category Genesis
 * @package Admin
 * @subpackage Inpost-Metaboxes
 *
 * @since 0.2.2
 *
 * @see genesis_inpost_layout_box() Generates the content in the boxes
 *
 * @return null Returns null if Genesis layouts are not supported
 */
function be_add_inpost_layout_box() {

	if ( ! current_theme_supports( 'genesis-inpost-layouts' ) )
		return;

	foreach ( (array) get_post_types( array( 'public' => true ) ) as $type ) {
		if ( post_type_supports( $type, 'genesis-layouts' ) )
			add_meta_box( 'genesis_inpost_layout_box', __( 'Layout Settings', 'genesis' ), 'genesis_inpost_layout_box', $type, 'normal', 'default' );
	}

}

/**
 * Remove Genesis widgets
 *
 * @since 1.0.0
 */
function be_remove_genesis_widgets() {
    unregister_widget( 'Genesis_eNews_Updates'          );
    unregister_widget( 'Genesis_Featured_Page'          );
    unregister_widget( 'Genesis_Featured_Post'          );
    unregister_widget( 'Genesis_Latest_Tweets_Widget'   );
    unregister_widget( 'Genesis_User_Profile_Widget'    );
}

/**
 * Remove Genesis Theme Settings Metaboxes
 *
 * @since 1.0.0
 * @param string $_genesis_theme_settings_pagehook
 */
function be_remove_genesis_metaboxes( $_genesis_theme_settings_pagehook ) {
	//remove_meta_box( 'genesis-theme-settings-feeds',      $_genesis_theme_settings_pagehook, 'main' );
	//remove_meta_box( 'genesis-theme-settings-header',     $_genesis_theme_settings_pagehook, 'main' );
	remove_meta_box( 'genesis-theme-settings-nav',        $_genesis_theme_settings_pagehook, 'main' );
	// remove_meta_box( 'genesis-theme-settings-layout',    $_genesis_theme_settings_pagehook, 'main' );
	//remove_meta_box( 'genesis-theme-settings-breadcrumb', $_genesis_theme_settings_pagehook, 'main' );
	//remove_meta_box( 'genesis-theme-settings-comments',   $_genesis_theme_settings_pagehook, 'main' );
	//remove_meta_box( 'genesis-theme-settings-posts',      $_genesis_theme_settings_pagehook, 'main' );
	remove_meta_box( 'genesis-theme-settings-blogpage',   $_genesis_theme_settings_pagehook, 'main' );
	//remove_meta_box( 'genesis-theme-settings-scripts',    $_genesis_theme_settings_pagehook, 'main' );
}

/**
 * Don't Update Theme
 * @since 1.0.0
 *
 * If there is a theme in the repo with the same name,
 * this prevents WP from prompting an update.
 *
 * @author Mark Jaquith
 * @link http://markjaquith.wordpress.com/2009/12/14/excluding-your-plugin-or-theme-from-update-checks/
 *
 * @param array $r, request arguments
 * @param string $url, request url
 * @return array request arguments
 */

function be_dont_update_theme( $r, $url ) {
	if ( 0 !== strpos( $url, 'http://api.wordpress.org/themes/update-check' ) )
		return $r; // Not a theme update request. Bail immediately.
	$themes = unserialize( $r['body']['themes'] );
	unset( $themes[ get_option( 'template' ) ] );
	unset( $themes[ get_option( 'stylesheet' ) ] );
	$r['body']['themes'] = serialize( $themes );
	return $r;
}

// ** Frontend Functions ** //

/**
 * Viewport Meta Tag for Mobile Browsers
 *
 * @author Bill Erickson
 * @link http://www.billerickson.net/code/responsive-meta-tag
 */
function be_viewport_meta_tag() {
	echo '<meta name="viewport" content="width=device-width, initial-scale=1.0"/>';
}

/**
 * Footer
 *
 */
function be_footer() {
	genesis_widget_area( 'footer', array(
	'before' => '<div class="footer">',
	'after'  => '</div>',
	) );
	genesis_widget_area( 'social_footer', array(
	'before' => '<div class="social_footer">',
	'after'  => '</div>',
	) );
	genesis_widget_area( 'copyrights', array(
	'before' => '<div class="copyrights">',
	'after'  => '</div>',
	) );
}


/* CUSTOM FUNCTIONS */
add_action( 'widgets_init', 'genesischild_extra_widgets' );
//Register in new Widget areas
function genesischild_extra_widgets() {
	genesis_register_sidebar( array(
	'id'            => 'header-middle',
	'name'          => 'Header Middle'
	) );
	genesis_register_sidebar( array(
	'id'            => 'footer',
	'name'          => 'Footer'
	) );
	genesis_register_sidebar( array(
	'id'            => 'social_footer',
	'name'          => 'Social Footer'
	) );
	genesis_register_sidebar( array(
	'id'            => 'copyrights',
	'name'          => 'Copyrights'
	) );
}

/*
 * Set-up Pre-defined Import Files for Customizer, Content, and Widgets
 */
function ocdi_import_files() {
	return array(
		array(
			'import_file_name'             => 'Theme Setup',
			//'categories'                   => array( 'Category 1', 'Category 2' ),
			'local_import_file'            => trailingslashit( get_stylesheet_directory() ) . 'preload-content/safe-states-preload-content.xml',
			'local_import_widget_file'     => trailingslashit( get_stylesheet_directory() ) . 'preload-content/safe-states-widgets.wie',
			'local_import_customizer_file' => trailingslashit( get_stylesheet_directory() ) . 'preload-content/safe-states-customizer.dat',

			//'import_preview_image_url'     => 'http://www.your_domain.com/ocdi/preview_import_image1.jpg',
			//'import_notice'                => __( 'After you import this demo, you will have to setup the slider separately.', 'your-textdomain' ),
			//'preview_url'                  => 'http://www.your_domain.com/my-demo-1',
		),
	);
}
add_filter( 'pt-ocdi/import_files', 'ocdi_import_files' );